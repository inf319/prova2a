package br.unicamp.ic.inf319;
/**
 * <img src="./doc-files/PiecePart.png" alt="PiecePart">
 *
 * @author INF319
 */
public class PiecePart extends Part {

    private double cost;
    
    /**
     *
     * @param thePartNumber
     * @param theDescription
     * @param theCost
     */
    public PiecePart(PartNumber thePartNumber, String theDescription,
                     double theCost) {
        super(thePartNumber, theDescription);
        cost = theCost;
    }

    /**
     *
     * @return cost
     */
    @Override
    public double cost() {
        return cost;
    }
    
    /**
     *
     * @return String list
     */
    @Override
    public String list() {
    	StringBuilder list = new StringBuilder(super.list());
    	list.append(" Custo: ").append(((Double) cost()).toString()).append("\n");
        return list.toString();
    }
    
    /**
     *
     * @param c
     */
    public void setCost(double c) {
    	cost = c;
    }
}
