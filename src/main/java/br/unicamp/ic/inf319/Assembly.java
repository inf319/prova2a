package br.unicamp.ic.inf319;

import java.util.Collections;

import java.util.LinkedList;
import java.util.List;
import java.util.Iterator;

/**
 * <img src="./doc-files/Assembly.png" alt="Assembly">
 *
 * @author INF319
 */
public class Assembly extends Part {

    private final List<Part> parts;

    /**
     *
     * @param thePartNumber
     * @param theDescription
     */
    public Assembly(PartNumber thePartNumber, String theDescription) {
        super(thePartNumber, theDescription);
        parts = new LinkedList<>();
    }

    /**
     *
     * @return cost
     */
    @Override
    public double cost() {
        double totalCost = 0;
        for (Iterator<Part> i = parts.iterator(); i.hasNext();) {
            Part part = i.next();
            totalCost += part.cost();
        }
// loop-for
//        for (Part part : parts) {
//            totalCost += part.cost();
//        }
//        totalCost = parts.stream().map(part -> part.cost()).reduce(totalCost, (accumulator, _item) -> accumulator + _item);
        return totalCost;
    }

    /**
     *
     * @param thePart
     */
    public void addPart(Part thePart) {
        parts.add(thePart);
    }

    /**
     *
     * @return parts
     */
    public List<Part> getParts() {
        return parts;
    }

    /**
     *
     * @return String list
     */
    @SuppressWarnings("unchecked")
    @Override
    public String list() {
        StringBuilder list = new StringBuilder(super.list());
        list.append(" Custo: ").append(((Double) cost()).toString()).append("\n");
        list.append(' ');
        Collections.sort(parts);
        for (Iterator<Part> i = parts.iterator(); i.hasNext();) {
            Part part = i.next();
            String partList = part.list().replaceAll("\n", "\n ");
            list.append(partList);
        }
// loop-for
//        for (Part part : parts) {
//            String partList = part.list().replaceAll("\n", "\n ");
//            list.append(partList);
//        }

//        parts.stream().map(part -> part.list().replaceAll("\n", "\n ")).forEachOrdered(partList -> {
//            list.append(partList);
//        });
        list.deleteCharAt(list.length() - 1);
        return list.toString();
    }
}
