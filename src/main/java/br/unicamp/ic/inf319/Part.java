package br.unicamp.ic.inf319;

import java.util.Objects;

/**
 * Por que a implementacao de list() nao segue exatamente o mesmo padrao de projeto-implementacao
 * de cost()? O que justifica as diferencas entre a implementacao de list() e cost() ?
 *
 * Resp..
 * As implementações são diferentes pois para o método cost() a classe Part não possui nenhuma informação 
 * sobre os custos, este método depende das implementações concretas das classes que 
 * herdam de Part que são Assembly e PiecePart.
 * O método list() é diferente, pois Part possui informações sobre partNumber e description. Portanto, 
 * a classe Part pode contribuir com o método incluindo informações que ela possui para o método. As classes 
 * filhas chamam o método list() de pai (Part) e simplesmente fazem um 'append' com as informações que elas 
 * possuem.
 *
 * @author INF319
 */
@SuppressWarnings("rawtypes")
public abstract class Part implements Comparable {

    private String description;
    private final PartNumber partNumber;
    
    /**
     *
     * @param thePartNumber
     * @param theDescription
     */
    public Part(PartNumber thePartNumber, String theDescription) {
        description = theDescription;
        partNumber = thePartNumber;
    }
    
    /**
     *
     * @return cost
     */
    public abstract double cost();
    
    /**
     *
     * @return list
     */
    public String list() {
    	return "Parte: " + partNumber.getNumber() + "; Descricao: " +
                description + ";";
    }

    /**
     *
     * @return description
     */
    public String getDescription() {
    	return description;
    }
    
    /**
     *
     * @param desc
     */
    public void setDescription(String desc) {
    	description = desc;
    }

    /**
     *
     * @return partNumber
     */
    public PartNumber getPartNumber() {
        return partNumber;
    }
    /**
     * 
     * @param o
     * @return equals
     */
    @Override
    public boolean equals(Object o) {
    	if (o instanceof Part) {
    	Part part = (Part) o;
    	return partNumber.getNumber() == part.partNumber.getNumber();
    	}
    	return false;
    }
    /**
     * 
     * @return hashCode
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.partNumber);
        return hash;
    }

    /**
     * 
     * @param o
     * @return compareTo
     */
    @Override
    public int compareTo(Object o) {
        if (partNumber.getNumber() > ((Part) o).partNumber.getNumber()) {
            return 1;
        } else if (partNumber.getNumber() < ((Part) o).partNumber.getNumber()) {
            return -1;
        }
        return 0;
    }
    
    
}
